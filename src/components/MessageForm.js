import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const MessageForm = ({ classes, createMessage }) => {
  const [body, setBody] = useState('');

  const handleSubmit = event => {
    event.preventDefault();
    createMessage(body);
    setBody('');
  }

  const handleChange = event => {
    setBody(event.target.value);
  }

  return (
    <Card className={classes.messageForm}>
      <CardContent>
        <form onSubmit={handleSubmit}>
          <FormControl fullWidth>
            <TextField 
            value={body}
            onChange={handleChange}
            className={classes.textarea}
            placeholder="Insert message..."
            rows={3}
            fullWidth 
            multiline />
          </FormControl>

          <Button 
          type="submit"
          variant="contained" 
          color="primary">Submit</Button>
        </form>
      </CardContent>
    </Card>
  )
}

export default MessageForm;