import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core';
import { compose } from 'recompose';
import { withSnackbar } from 'notistack';
import Grid from '@material-ui/core/Grid';

import styles from './app.css';
import io from 'socket.io-client';

import User from './User';
import Users from './Users';
import JoinForm from './JoinForm';
import Messages from './Messages';
import MessageForm from './MessageForm';

let api = ':3000';

if (process.env.NODE_ENV === 'production') {
  api = 'https://gentle-escarpment-77331.herokuapp.com/'
}

class App extends Component {
  state = {
    user: null,
    users: [],
    messages: []
  }

  componentDidMount = () => {
    this.io = io(api);
    this.joinBack();

    this.io.on('users', data => {
      this.setState({ users: data.users })
    })

    this.io.on('messages', data => {
      this.setState({ messages: data.messages })
    })

    this.io.on('user joined', data => {
      this.setState(({ users }) => ({
        users: [ ...users, data.user ]
      }))
    })

    this.io.on('user left', data => {
      this.setState(({ users }) => ({
        users: users.filter(({ username }) => {
          return username !== data.user.username
        })
      }))
    })

    this.io.on('message created', data => {
      this.setState(({ messages }) => ({
        messages: [ ...messages, data.message ]
      }))
    })

    this.io.on('me joined', ({ user }) => {
      localStorage.setItem('user', JSON.stringify(user));
      this.setState({ user })
    })

    this.io.on('exists', ({ user }) => {
      this.setState({ user });
    })

    this.io.on('my_error', ({ message }) => {
      this.props.enqueueSnackbar(message, {
        variant: 'error',
      });
    })
  }

  throwError = message => {
    this.props.enqueueSnackbar(message, { variant: 'error' })
  }

  join = username => {
    if(!username) {
      this.throwError('Must provide username');
    }

    this.io.emit('join', { username });
  }

  joinBack = () => {
    const user = JSON.parse(localStorage.getItem('user'));

    if(user) {
      this.io.emit('join', { username: user.username })
    }
  }

  createMessage = (body) => {
    const author = this.state.user.username;
    this.io.emit('message', { body, author });
  }

  leaveChat = () => {
    this.io.emit('leaving');
    localStorage.removeItem('user');
    this.setState({ user: null })
  }

  render() {
    const { classes } = this.props;
    const { users, user, messages } = this.state;

    return (
      <Grid spacing={4} justify="center" container className={classes.app}>
        {
          user ? (
            <Fragment>
              <Grid className={classes.side} lg={4} md={6} sm={12} xs={12} item>
                <User 
                handleLeaveChat={this.leaveChat}
                classes={classes} 
                user={user} />

                <Users 
                users={users} 
                classes={classes} />
              </Grid>

              <Grid className={classes.side} lg={5} md={6} sm={12} xs={12} item>
                <MessageForm 
                createMessage={this.createMessage}
                classes={classes} />

                <Messages
                messages={messages}
                classes={classes} />
              </Grid>
            </Fragment>
          ) : (
            <Grid lg={4} md={6} sm={8} item>
              <JoinForm 
              username={user}
              joinChat={this.join}
              classes={classes} />
            </Grid>
          )
        }
      </Grid>
    )
  }
}

const enhance = compose(
  withSnackbar,
  withStyles(styles)
)

export default enhance(App);