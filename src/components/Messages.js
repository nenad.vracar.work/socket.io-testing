import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';

const MessagesList = ({ messages, classes }) => 
  messages.map((message, index) => (
    <ListItem key={index} className={classes.message}>
      <ListItemText
      primary={message.body}
      secondary={message.author} />
    </ListItem>
  ))


const Messages = ({ classes, messages }) => (
  <Card className={classes.messages}>
    <List className={classes.messagesList}>
      <ListSubheader classes={{
        root: classes.subHeader
      }}>Messages</ListSubheader>
      <Divider />
      <MessagesList 
      classes={classes} 
      messages={messages} />
    </List>
  </Card>
)

export default Messages;