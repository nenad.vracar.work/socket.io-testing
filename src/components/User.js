import React from 'react';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

const User = ({ user, classes, handleLeaveChat }) => {
  return (
    <Card className={classes.user}>
      <CardContent className={classes.userContent}>
        <Typography color="textSecondary">
          Your username: 
        </Typography>

        <Typography variant="body1" component="span">
          {user.username}
        </Typography>
      </CardContent>

      <CardContent>
        <Button 
        color="default" 
        onClick={handleLeaveChat}
        variant="contained">Leave</Button>
      </CardContent>
    </Card>
  )
}

export default User;