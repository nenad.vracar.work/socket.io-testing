import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';

const UsersList = ({ users, classes }) => 
  users.map((user, index) => (
    <ListItem key={index} className={classes.userItem}>
      <ListItemText
      primary={user.username}
      secondary={user.id} />
    </ListItem>
  ))


const Users = ({ classes, users }) => (
  <Card className={classes.users}>
    <List className={classes.userList}>
      <ListSubheader classes={{
        root: classes.subHeader
      }}>Users</ListSubheader>
      <Divider />
      <UsersList 
      classes={classes} 
      users={users} />
    </List>
  </Card>
)

export default Users;