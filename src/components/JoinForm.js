import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const JoinForm = ({ classes, joinChat }) => {
  const [username, setUsername] = useState('');
  
  const handleChange = event => {
    setUsername(event.target.value);
  }

  return (
    <Card className={classes.heading}>
      <CardContent className={classes.headingContent}>
        <FormControl fullWidth size="small" className={classes.formControl}>
          <TextField
          fullWidth
          value={username}
          onChange={handleChange} 
          className={classes.joinTextField}
          label="Type username..." />

          <Button 
          color="primary" 
          onClick={() => joinChat(username)}
          variant="contained">Join</Button>
        </FormControl>
      </CardContent>
    </Card>
  )
}

export default JoinForm;