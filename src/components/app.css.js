export default theme => ({
  app: {
    height: '100vh',
    padding: theme.spacing(2),
    maxWidth: '100%',
    margin: 0
  },
  user: {
    marginBottom: theme.spacing(4),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  userContent: {
    flex: 1
  },
  users: {
    flex: 1
  },
  heading: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 25,
    width: '100%'
  },
  headingContent: {
    width: '100%'
  },
  formControl: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    width: '100%'
  },
  username: {
    display: 'flex',
    alignItems: 'center',

    '& :last-child': {
      marginLeft: 10
    }
  },
  subHeader: {
    background: 'white'
  },
  messages: {
    maxHeight: '100%',
    display: 'flex',
    flexDirection: 'column',
    flex: 1
  },
  messageForm: {
    marginBottom: theme.spacing(4)
  },
  messagesList: {
    overflowY: 'auto',
    maxHeight: '100%'
  },
  userList: {
    overflowY: 'auto',
    maxHeight: '100%'
  },
  textarea: {
    marginBottom: 10
  },
  joinTextField: {
    flex: 1
  },
  side: {
    display: 'flex',
    maxHeight: '100%',
    flexDirection: 'column'
  }
});
